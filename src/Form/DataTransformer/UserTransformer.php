<?php


namespace App\Form\DataTransformer;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }


    /**
     * Transforms an object (User) to a string (number).
     *
     * @param  User|null $issue
     * @return string
     */
    public function transform($user)
    {
        if ($user === null)
            return '';
        return $user->getId();
    }

    /**
     * Transforms a string (number) to an object (User).
     *
     * @param  string $issueNumber
     * @return User|null
     * @throws TransformationFailedException if object (user) is not found.
     */
    public function reverseTransform($userId)
    {
        if (!$userId)
            return null;

        $user = $this->em
            ->getRepository(User::class)
            ->find($userId);

        if (!$user) {
            throw new TransformationFailedException(sprintf(
                'An user with id "%s" does not exist!',
                $userId
            ));
        }
        return $user;
    }
}