<?php

namespace App\Controller\ApiAdmin;

use App\Controller\BaseApiController;
use App\Entity\User;
use App\Form\UserType;
use App\Service\FormHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends BaseApiController
{
    protected function defaultGroups(): array
    {
        return ['admin'];
    }

    /**
     * Edit an user
     *
     * @param Request $request
     * @param FormHandler $formHandler
     * @return Response
     */
    public function edit(Request $request, FormHandler $formHandler): Response
    {
        /**
         * @var User $user
         */
        $user = $this->em->getRepository(User::class)
            ->find($request->get('id'));

        if (!$user) {
            throw $this->notFoundException();
        }

        try {
            $form = $this->getForm(UserType::class, $user);
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $this->persistFlush($user);
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->handleView($this->view($user));
    }

    /**
     * Remove an user
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        /**
         * @var User $user
         */
        $user = $this->em->getRepository(User::class)
            ->find($request->get('id'));

        if (!$user) {
            throw $this->notFoundException();
        }

        $id = $user->getId();
        $username = $user->getUsername();

        $this->removeAndFlush($user);

        return $this->handleView(
            $this->view(['Remove User ' . $id . ' (' . $username . ') succesfully'])
        );

    }
}