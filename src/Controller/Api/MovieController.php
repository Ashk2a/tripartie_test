<?php

namespace App\Controller\Api;

use App\Controller\BaseApiController;
use App\Entity\Movie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MovieController extends BaseApiController
{
    protected function defaultGroups(): array
    {
        return ($this->getUser()) ? ['user'] : [];
    }

    /**
     * List all movies in database
     *
     * @return Response
     */
    public function list(): Response
    {
        /**
         * @var Movie[] $movies
         */
        $movies = $this->em->getRepository(Movie::class)
            ->buildQuery($this->getParamHandler())
            ->getAll();
        return $this->resourceOkResponse($movies);
    }

    /**
     * Get a movie by id
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request): Response
    {
        $movie = $this->em->getRepository(Movie::class)
            ->buildQuery($this->getParamHandler())
            ->getOne($request->get('id'));

        if (!$movie) {
            throw $this->notFoundException();
        }

        return $this->resourceOkResponse($movie);
    }
}