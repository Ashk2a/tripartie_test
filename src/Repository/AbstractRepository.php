<?php


namespace App\Repository;


use App\Service\ParamHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractRepository extends ServiceEntityRepository
{
    /**
     * @var ParamHandler
     */
    private $handler;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder = null;

    /**
     * @var string
     */
    private static $tableAlias = 'q'; //like Query!

    /**
     * A preprocess command, build a query with given params
     * You need to execute :
     * $this->getOne(int $id)
     * $this->getAll()
     *
     * @param ParamHandler $handler
     * @return AbstractRepository
     */
    public function buildQuery(ParamHandler $handler): self
    {
        $this->handler = $handler;
        // Process params given in $handler
        $fields = $this->processFields();
        $order = $this->procesOrderBy();
        // Set formatted params on query
        $this->queryBuilder = $this->createQueryBuilder(self::$tableAlias)
            ->select($fields)
            ->orderBy($order[0], $order[1]);
        return $this;
    }

    /**
     * Return one resource by id.
     * You need to preprocess method $this->buildQuery(ParamHandler $handler)
     *
     * @param int $id
     * @return mixed|null
     */
    public function getOne(int $id)
    {
        if ($this->queryBuilder == null) {
            return null;
        }
        // Set Resource id in query
        $this->queryBuilder->where('q.id = :id');
        $this->queryBuilder->setParameter('id', $id);

        try {
            $result = $this->queryBuilder->getQuery()->getOneOrNullResult();
            $this->queryBuilder = null;

            return $result;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * Return a resource collection.
     * You need to preprocess method $this->buildQuery(ParamHandler $handler)
     *
     * @return array
     */
    public function getAll()
    {
        if ($this->queryBuilder == null)
            return [];
        $results = $this->queryBuilder->getQuery()->getResult();
        $this->queryBuilder = null;

        return $results;
    }

    /**
     * @return array
     */
    private function processFields(): array
    {
        $fields = $this->handler->getParam('fields');
        // if format "1,2,3,4..." then to [1,2,3,4..]
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        // Remove value from $fields if not exist
        $fields = array_filter($fields, function ($field) {
            return $this->getClassMetadata()->hasField(trim($field));
        });
        // Format well the $fields, concat with alias, ex : id => q.id
        $fields = array_map(function ($field) {
            $formatted = trim($field);
            $formatted = self::$tableAlias . "." . $formatted;

            return $formatted;
        }, $fields);

        return $fields;
    }

    private function procesOrderBy(): array
    {
        $default = ['q.id', 'asc'];
        $orderBy = $this->handler->getParam('order');
        $columns = $this->getClassMetadata()->getColumnNames();

        if (is_array($orderBy)) {
            return $default;
        }

        $orderBy = explode(',', $orderBy);

        if (count($orderBy) != 2) {
            return $default;
        }

        if (!in_array($orderBy[0], $columns)) {
            return $default;
        }

        $orderBy[0] = self::$tableAlias . "." . $orderBy[0];

        if (!in_array($orderBy[1], ['asc', 'desc'])) {
            return $default;
        }

        return $orderBy;
    }

}