<?php


namespace App\Service;


use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class FormHandler
{
    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var FormInterface $form
     */
    private $form;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * Process the $form given, return if he is valide
     *
     * @param FormInterface $form
     *
     * @return bool
     */
    public function process(FormInterface $form) : bool
    {
        $this->form = $form;

        $data = json_decode($this->request->getContent(), true);
        $clearMissing = false;
        $form->submit($data, $clearMissing);
        return $form->isValid();
    }

    /**
     * Return error toString
     *
     * @return string
     */
    public function error()
    {
        $attr_name = $this->form
            ->getErrors(true)
            ->current()
            ->getOrigin()
            ->getName();
        $attr_msg_error = $this->form
            ->getErrors(true)
            ->current()
            ->getMessage();
        return "`" . $attr_name . "` - " . $attr_msg_error;
    }
}