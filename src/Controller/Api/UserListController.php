<?php

namespace App\Controller\Api;

use App\Controller\BaseApiController;
use App\Entity\Movie;
use App\Entity\User;
use App\Entity\UserList;
use App\Form\UserListType;
use App\Service\FormHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Default route for UserList Controller
 *
 */
class UserListController extends BaseApiController
{
    protected function defaultGroups(): array
    {
        return ($this->getUser()) ? ['user'] : [];
    }

    /**
     * List all users lists
     *
     * @return Response
     */
    public function list(): Response
    {
        /**
         * @var UserList[] $lists
         */
        $lists = $this->em->getRepository(UserList::class)
            ->buildQuery($this->getParamHandler())
            ->getAll();

        return $this->handleView($this->view($lists));
    }

    /**
     * Get a list by id
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request): Response
    {
        /**
         * @var UserList $list
         */
        $list = $this->em->getRepository(UserList::class)
            ->buildQuery($this->getParamHandler())
            ->getOne($request->get('id'));

        if (!$list) {
            throw $this->notFoundException();
        }

        return $this->handleView($this->view($list));
    }

    /**
     * Create a list
     *
     * @param FormHandler $formHandler
     * @return Response
     */
    public function create(FormHandler $formHandler): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $list = new UserList();

        try {
            $form = $this->getForm(UserListType::class, $list);
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $user->addList($list);

            $this->persistFlush($user);
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->resourceCreateResponse($list);
    }

    /**
     * Edit a list
     *
     * @param Request $request
     * @param FormHandler $formHandler
     * @return Response
     */
    public function edit(Request $request, FormHandler $formHandler)
    {
        /**
         * @var User $user
         * @var UserList $list
         */
        $user = $this->getUser();
        $list = $this->em->getRepository(UserList::class)
            ->find($request->get('id'));

        if (!$list) {
            throw $this->notFoundException();
        }
        if (!$user->getLists()->contains($list)) {
            throw  new AccessDeniedException("You can't edit this list");
        }

        try {
            $form = $this->getForm(UserListType::class, $list);
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $this->persistFlush($list);
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->resourceOkResponse($list);
    }

    /**
     * Add an existing movie to a list
     *
     * @param Request $request
     * @return Response
     */
    public function addMovie(Request $request)
    {
        /**
         * @var UserList $list
         * @var Movie $movie
         * @var User $user
         */
        $list = $this->em->getRepository(UserList::class)
            ->find($request->get('id'));
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($request->get('movie_id'));
        $user = $this->getUser();

        if (!$list) {
            throw $this->notFoundException();
        }
        if (!$user->getLists()->contains($list)) {
            throw $this->accessDeniedException("This is not your list!");
        }
        if (!$movie) {
            throw $this->notFoundException('Movie ' . $request->get('movie_id') . ' not found');
        }
        if ($list->getMovies()->contains($movie)) {
            throw $this->conflictException('Movie ' . $movie->getId() . ' (' . $movie->getTitle() . ') already in this list');
        }

        $list->addMovie($movie);

        $this->persistFlush($list);

        return $this->resourceOkResponse($list);
    }

    /**
     * Remove a movie from a list
     *
     * @param Request $request
     * @return Response
     */
    public function removeMovie(Request $request)
    {
        /**
         * @var UserList $list
         * @var Movie $movie
         * @var User $user
         */
        $list = $this->em->getRepository(UserList::class)
            ->find($request->get('id'));
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($request->get('movie_id'));
        $user = $this->getUser();

        if (!$list) {
            throw $this->notFoundException();
        }
        if (!$user->getLists()->contains($list)) {
            throw  $this->accessDeniedException("You can't edit this list ");
        }
        if (!$movie) {
            throw $this->notFoundException('Movie ' . $request->get('movie_id') . ' not found');
        }
        if (!$list->getMovies()->contains($movie)) {
            throw $this->notFoundException('Movie ' . $request->get('movie_id') . ' not in this list');
        }

        $list->removeMovie($movie);

        $this->persistFlush($list);

        return $this->resourceDeleteResponse($list);
    }

    /**
     * Delete a list
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        $list = $this->em->getRepository(UserList::class)
            ->find($request->get('id'));

        if (!$list) {
            throw $this->notFoundException();
        }

        /**
         * @var User $user
         */
        $user = $this->getUser();

        if (!$user->getLists()->contains($list)) {
            throw $this->accessDeniedException("You can't edit this list ");
        }

        $id = $list->getId();
        $name = $list->getName();

        $user->removeList($list);

        $this->persistFlush($user);

        return $this->resourceDeleteResponse($list);
    }
}