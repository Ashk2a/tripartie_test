<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190911161010 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_list_movie DROP FOREIGN KEY FK_63AAA0803DAE168B');
        $this->addSql('ALTER TABLE user_list_movie ADD CONSTRAINT FK_63AAA0803DAE168B FOREIGN KEY (list_id) REFERENCES user_list (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_list_movie DROP FOREIGN KEY FK_63AAA0803DAE168B');
        $this->addSql('ALTER TABLE user_list_movie ADD CONSTRAINT FK_63AAA0803DAE168B FOREIGN KEY (list_id) REFERENCES user_list (id)');
    }
}
