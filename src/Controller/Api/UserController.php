<?php

namespace App\Controller\Api;

use App\Controller\BaseApiController;
use App\Entity\User;
use App\Form\UserType;
use App\Service\FormHandler;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends BaseApiController
{
    protected function defaultGroups(): array
    {
        return ($this->getUser()) ? ['user'] : [];
    }

    /**
     * List all users
     *
     * @return Response
     */
    public function list(): Response
    {
        /**
         * @var User[] $users
         */
        $users = $this->em->getRepository(User::class)
            ->buildQuery($this->getParamHandler())
            ->getAll();
        $this->setGroup('user_det');

        return $this->handleView($this->view($users));
    }

    /**
     * Get one user by id
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request): Response
    {
        /**
         * @var User $user
         */
        $user = $this->em->getRepository(User::class)
            ->buildQuery($this->getParamHandler())
            ->getOne($request->get('id'));

        if (!$user) {
            throw $this->notFoundException();
        }

        return $this->handleView($this->view($user));
    }

    /**
     * Create an user
     *
     * @param UserPasswordEncoderInterface $encoder
     * @param FormHandler $formHandler
     * @return Response
     */
    public function create(UserPasswordEncoderInterface $encoder, FormHandler $formHandler): Response
    {
        $user = new User();

        try {
            $form = $this->getForm(
                UserType::class,
                $user,
                ['validation_groups' => ['registration']]
            );
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $user->setRole('ROLE_USER');

            $this->persistFlush($user);
        } catch (Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        $this->setGroup('user');

        return $this->resourceCreateResponse($user);
    }

    /**
     * Edit an user
     *
     * @param UserPasswordEncoderInterface $encoder
     * @param FormHandler $formHandler
     * @return Response
     */
    public function edit(UserPasswordEncoderInterface $encoder, FormHandler $formHandler)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        try {
            $form = $this->getForm(
                UserType::class,
                $user
            );
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            if ($user->getPlainPassword() != null) {
                $user->setPassword(
                    $encoder->encodePassword(
                        $user, $user->getPlainPassword()
                    )
                );
            }

            $this->persistFlush($user);
        } catch (Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->resourceOkResponse($user);
    }
}