<?php

namespace App\Service;

use Doctrine\Common\Annotations\Reader;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Request\ParamReader;
use ReflectionClass;

/**
 * More flexible ParamFecther
 * - inherit annotations from parent
 * - keep logic with serializer groups
 * - automatic gestion of params fields, where, order..
 *
 * Class ParamHandler
 * @package App\Service
 */
class ParamHandler
{
    /**
     * @var ParamFetcher $paramFetcher
     */
    private $paramFetcher;

    /**
     * Serializer groups
     * @var array
     */
    private $groups;

    /**
     * @var Reader $reader
     */
    private $reader;

    /**
     * State, if true params between parent and child has been merged
     * @var bool $paramsMerged ;
     */
    private $merged = false;

    /**
     * @var array $parentsClasses
     */
    private $parentsClasses = [];

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * Link the request ParamFetcherInterface
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return ParamHandler
     */
    public function setParamFetcher(ParamFetcherInterface $paramFetcher): self
    {
        $this->paramFetcher = $paramFetcher;

        return $this;
    }

    /**
     * Manual link parent::class annotations here
     *
     * @param array $parentsClasses
     *
     * @return ParamHandler
     */
    public function setParentsClasses(array $parentsClasses): self
    {
        $this->parentsClasses = $parentsClasses;

        return $this;
    }

    public function getGroups() {
        return $this->groups;
    }

    /**
     * Set the serializer groups to keep the same logic inside the query
     * Example : user can't see attribute password.
     * So if he try to do in query "..where password..." query will ignore it
     *
     * @param $groups
     * @return ParamHandler
     */
    public function setGroups($groups) : self
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasParam(string $name): bool
    {
        $this->mergeParentsAnnotations();
        return key_exists($name, $this->getParams());
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $this->mergeParentsAnnotations();
        return $this->paramFetcher->all();
    }

    /**
     * Get all params with details
     *
     * @return ParamInterface[]
     */
    public function getParams()
    {
        $this->mergeParentsAnnotations();
        return $this->paramFetcher->getParams();
    }

    /**
     * @param string $name
     *
     * @return array|string
     */
    public function getParam(string $name)
    {
        $this->mergeParentsAnnotations();
        return $this->paramFetcher->get($name);
    }

    /**
     * Merge all parents annotations parans with child
     *
     * @return ParamHandler
     */
    private function mergeParentsAnnotations(): self
    {
        if (!$this->merged) {
            $reader = new ParamReader($this->reader);
            foreach ($this->parentsClasses as $parent) {
                try {
                    $reflexion = new ReflectionClass($parent);
                    foreach ($reader->getParamsFromClass($reflexion) as $param) {
                        $this->paramFetcher->addParam($param);
                    }
                } catch (\ReflectionException $e) {

                }
            }
            $this->merged = true;
        }
        return $this;
    }
}