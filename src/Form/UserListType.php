<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/08/2019
 * Time: 15:04
 */

namespace App\Form;

use App\Entity\User;
use App\Entity\UserList;
use App\Form\DataTransformer\UserTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Config\Definition\IntegerNode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserListType extends AbstractType
{
    /**
     * @var UserTransformer $em
     */
    private $userTranformer;

    public function __construct(UserTransformer $userTransformer)
    {

        $this->userTranformer = $userTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('movies', CollectionType::class, [
                'entry_type' => MovieType::class,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserList::class,
            'csrf_protection' => false
        ));
    }
}